package main

const (
	UPLOAD_IMPL string = `
        # Install public ssh public key to authorized_keys
        # @author progrium
        # @link https://github.com/progrium/keychain.io
        set -e
        keypath="{{.keypath}}"
        if [[ $keypath = "" ]]
        then
            if [ -e "$HOME/.ssh/id_dsa.pub" ]
            then
                keypath="$HOME/.ssh/id_dsa.pub"
            elif [ -e "$HOME/.ssh/id_rsa.pub" ]
            then
                keypath="$HOME/.ssh/id_rsa.pub"
            else
                echo "Unable to find a public key."
                echo "Specify a path with 'keypath' parameter in URL."
                exit 1  
            fi
        fi

        curl -s -X PUT -F key="@$keypath" "{{.url_root}}/{{.email}}/{{.keyname}}"
    `
	INSTALL_IMPL string = `
        # Upload ssh public key script
        # @author progrium
        # @link https://github.com/progrium/keychain.io
        mkdir -p $HOME/.ssh
        chmod 700 $HOME/.ssh
        touch $HOME/.ssh/authorized_keys
        chmod 600 $HOME/.ssh/authorized_keys
        
        echo "\r\n\r\n#{{.email}}" >> $HOME/.ssh/authorized_keys
        echo "{{.key}}" >> $HOME/.ssh/authorized_keys
    `
)

const (
	INSTALL_KEY string = "install"
	UPLOAD_KEY  string = "upload"
)
