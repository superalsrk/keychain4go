BINDATA_IGNORE = $(shell git ls-files -io --exclude-standard $< | sed 's/^/-ignore=/;s/[.]/[.]/g')

default: usage

usage:
	@echo ""
	@echo "Task                 : Description"
	@echo "-----------------    : -------------------"
	@echo "make setup           : Install all necessary dependencies"
	@echo "make test            : Run tests"
	@echo "make build           : Generate production build for current OS"
	@echo "make rundev"	        : Run dev Binary"
	@echo "make run"            : Run Binary"
	@echo "make clean           : Remove all build files and reset assets"
	@echo "make docker          : Build docker image"
	@echo ""

setup:
	go get github.com/tools/godep
	godep restore

test: 
	godep go test

build:
	@godep go build
	@echo "You can now execute ./keychain4go"
	
rundev:
	@go run *.go

run:
	@./keychain4go
clean:
	rm -rf ./keychain4go

docker:
	docker build -t keychain4go 