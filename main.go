package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jessevdk/go-flags"
	"os"
)

type Options struct {
	Version bool   `short:"v" long:"version" description:"Print version"`
	Host    string `short:"h" long:"host" description:"Server hostname" default:"localhost"`
	Port    int    `short:"p" long:"port" description:"Server port" default:"8000"`
	Folder  string `short:"f" long:"folder" description:"Auth Keys Folder" default:".keychain4go"`
	Confirm bool `short:"c" long:"confirm" description:"Enable email confirmation" default:"false"`
}

var options Options

const VERSION = "0.1.1"

func intOptions() {
	_, err := flags.ParseArgs(&options, os.Args)

	if err != nil {
		os.Exit(1)
	}

	if options.Version {
		fmt.Printf("keychain4go v%s\n", VERSION)
		os.Exit(0)
	}
}

func startServer() {
	router := gin.Default()

	setupRouters(router)
	router.Run(fmt.Sprintf("%s:%d", options.Host, options.Port))
}

func main() {
	intOptions()
	fmt.Printf("Keychain4go is starging server on %s:%d\n\n", options.Host, options.Port)
	startServer()

}
