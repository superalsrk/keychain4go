keychain4go
===

![MIT](https://img.shields.io/npm/l/express.svg) ![Release](https://img.shields.io/github/release/superalsrk/keychain4go.svg) [![Build Status](https://img.shields.io/travis/superalsrk/keychain4go.svg)](https://travis-ci.org/superalsrk/keychain4go)


 One way to manage ssh public keys, and this repo is greatly inspired by [keychain.io](https://github.com/progrium/keychain.io)


##Install
1. `go get github.com/superalsrk/keychain4go`

2. Run command **keychain4go** to start your server

```
Usage:
  keychain4go [OPTIONS]

Application Options:
  -v, --version  Print version
  -h, --host=    Server hostname (localhost)
  -p, --port=    Server port (8080)
  -f, --folder=  Auth Keys Folder (~/.keychain4go)

Help Options:
  -h, --help     Show this help message
```

##Usage

Upload your default SSH key:

```
curl -s ssh.stackbox.cn/<email>/upload | bash
```

Install your key into authorized_keys:

```
curl -s ssh.stackbox.cn/<email>/install | bash
```

##APIs

```
ssh.stackbox.cn/<email>
ssh.stackbox.cn/<email>/upload
ssh.stackbox.cn/<email>/install
ssh.stackbox.cn<email>/fingerprint


//APIs below have not been implemented
ssh.stackbox.cn/<email>/all
ssh.stackbox.cn/<email>/confirm/<token>
ssh.stackbox.cn/<email>/all/install
ssh.stackbox.cn/<email>/<namedkey>
ssh.stackbox.cn/<email>/<namedkey>/fingerprint
ssh.stackbox.cn/<email>/<namedkey>/install
ssh.stackbox.cn/<email>/<namedkey>/upload
```