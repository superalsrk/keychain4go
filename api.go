package main

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
)

func setupRouters(router *gin.Engine) {

	router.GET("/:email/:action", func(c *gin.Context) {
		c.String(200, execute_action(c))
	})

	router.GET("/:email/:action/*keyname", func(c *gin.Context) {
		c.String(200, execute_action(c))
	})

	router.PUT("/:email/:keyname", func(c *gin.Context) {
		c.String(200, upload_key(c))
	})

}

func execute_action(c *gin.Context) string {
	email := c.Params.ByName("email")
	action := c.Params.ByName("action")
	keypath := c.Params.ByName("keypath")
	keyname := c.Params.ByName("keyname")

	if strings.HasPrefix(keyname, "/") {
		keyname = keyname[1:]
	}
	if strings.EqualFold(keyname, "") {
		keyname = "default"
	}

	var buf bytes.Buffer

	switch action {
	case UPLOAD_KEY:
		tmpl, _ := template.New(UPLOAD_KEY).Parse(UPLOAD_IMPL)
		tmpl.Execute(&buf, map[string]string{"email": email, "keypath": keypath, "url_root": c.Request.Host, "keyname": keyname})

	case INSTALL_KEY:
		tmpl, _ := template.New(INSTALL_KEY).Parse(INSTALL_IMPL)
		tmpl.Execute(&buf, map[string]string{"email": email, "key": read_key(c, keyname)})
	default:
		return "No Such Action!"
	}

	return buf.String()
}

func upload_key(c *gin.Context) string {

	file, _, err := c.Request.FormFile("key")

	if err != nil {
		return "form file error"
	}

	os.MkdirAll(options.Folder+"/"+c.Params.ByName("email"), 0777)

	fmt.Println(options.Folder + "/" + c.Params.ByName("email"))
	defer file.Close()
	out, err := os.Create(options.Folder + "/" + c.Params.ByName("email") + "/" + c.Params.ByName("keyname"))

	if err != nil {
		return "Unable to create the file for writing. Check your write access privilege"
	}

	defer out.Close()

	_, err = io.Copy(out, file)

	if err != nil {
		return "Copy file error"
	}

	return "File uploaded successfully"
}

func read_key(c *gin.Context, keyname string) string {

	filebuf, err := ioutil.ReadFile(options.Folder + "/" + c.Params.ByName("email") + "/" + keyname)
	if err != nil {
		return "read file error"
	}
	return string(filebuf)

}
